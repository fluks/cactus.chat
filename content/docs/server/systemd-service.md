---
title: "Self-Hosting with systemd"
description: "How to self-host the Cactus Comments as a systemd service"
lead: ""
date: 2022-04-06T08:49:31+00:00
lastmod: 2022-04-12T07:50:31+00:00
draft: false
images: []
menu:
  docs:
    parent: "server"
weight: 33
toc: true
---

## Running as a service

It is recommended to host `cactus-appservice` with docker as described [here](../self-host).

This page describes an experimental approach for running the appservice as a systemd service using Python's virtualenv. If you have any suggestions for how to improve this, please don't hesitate to [contribute](../../community/contribute)!

## Prerequisites

You need a host that uses systemd as a service manager. You'll also need Python 3.9 or newer and `pip`.

## Configuration

### Basic self-host configuration

Please follow the configuration steps of [the main self-host document](../self-host), except of course the `docker` part.

### Setup the appservice's directory

You need to decide where to store the appservice. Reasonable parent folder examples are `/opt`, `/srv/www`, `/var/www/`.
Assuming you are in the chosen folder, first clone the repo:

```bash
$ git clone https://gitlab.com/cactus-comments/cactus-appservice
$ cd cactus-appservice
```

Second, create a python virtualenv, and activate it:

```bash
$ virtualenv env
$ source env/bin/activate
```

Third, install the dependencies in the virtualenv:

```bash
$ pip install -r requirements.txt
```

You can leave the virtualenv now:

```bash
$ deactivate
```

### Adjust the environment file

Create the environment file as described in the [self host page](../self-host#cactus-comments-application-service).

To make the setup self-contained (and separate from the git repo), move the file to the `env` folder created by `virtualenv` (which is .gitignored), and name it `appservice.env`. It should be readable only by the user running systemd daemon: root!

```bash
$ mv <path-to-created-env-file> env/appservice.env
$ chown root. $_
$ chmod o-rwx $_
```

Next, set `$PATH` in the environment file, so that it points to `<chekout-path>/env/bin`.

The extended file could look like this (if you chose `/srv/www` as the repo's parent folder):

```toml
PATH=/srv/www/cactus-appservice/env/bin
CACTUS_HS_TOKEN=b3b05236568ab46f0d98a978936c514eac93d8f90e6d5cd3895b3db5bb8d788b
CACTUS_AS_TOKEN=a2d7789eedb3c5076af0864f4af7bef77b1f250ac4e454c373c806876e939cca
CACTUS_HOMESERVER_URL=http://localhost:8008
CACTUS_USER_ID=@cactusbot:example.com
```

### Copy and adjust systemd service file

There is a systemd unit file in the appservice repo, that is *almost* ready-to-use out of the box.

Install it so systemd can find it with `cp`:

```bash
$ sudo cp cactus-comments.example.service /etc/systemd/system/cactus-comments.service
```

You need to change the following parameters (marked by `# Adjust this!`) in the service file:
- set `EnvironmentFile` to the full path of `appservice.env` (the file you adjusted previously)
- set `WorkingDirectory` to the full path of the local appservice repo.

For example the new file's first lines should look like this (if you chose `/srv/www` as the repo's parent folder):

```toml
[Unit]
Description=Cactus Comments appservice: Matrix powered embeddable comment backend

[Service]                                                                                         
Type=simple
ExecStart=/bin/bash -c 'gunicorn -w 4 -b 127.0.0.1:5000 --timeout 500 "app:create_app_from_env()"'
Restart=always
# Adjust this!
EnvironmentFile=/srv/www/cactus-appservice/env/appservice.env
# Adjust this!
WorkingDirectory=/srv/www/cactus-appservice
...
```
  
Save your changes, then let systemd know about the new service:

```bash
$ sudo systemctl daemon-reload
```

## Start the service!

If you've also [configured synapse](../self-host#synapse), Cactus Comments should be ready to run as a service!
You can enable *and* start the service in one fell swoop:

```bash
$ sudo systemctl enable --now cactus-comments
```

Check the logs:

```bash
$ sudo journalctl -u cactus-comments
```

The last lines should be something like:

`Apr 06 01:04:27 example.com bash[5189]: [2022-04-06 01:04:27,041] INFO in app: Created application!`

Hooray! It runs!
